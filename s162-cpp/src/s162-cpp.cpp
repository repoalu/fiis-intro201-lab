//============================================================================
// Name        : s162-cpp.cpp
// Author      : 
// Version     :
// Copyright   : 
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;

void elementos(){
	int lista[4] = {1, 2, 3, 4};
	int n = 4;
	for(int i = 0; i < n; i++){
		cout << "Elemento: " << lista[i] << endl;
	}
}

void repetitivas(){
	int n = 100;
	int sumaPar = 0;
	int sumaImpar = 0;
	for(int i = 1; i < n + 1 ; i++){
		if(i % 2 == 0){
			sumaPar += i;
		}else{
			sumaImpar += i;
		}
	}
}

int multiplicar(int a, int b){
	return a * b;
}

string saludar(string nombre){
	string respuesta = "Hola " + nombre;
	return respuesta;
}


int main() {
	string res = saludar("Carlos");
	cout << res << endl;
}











