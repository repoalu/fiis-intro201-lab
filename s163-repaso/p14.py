# -*- coding: utf-8 -*-
'''
Dada una lista L, implemente un algoritmo que permita
determinar la suma de sus elementos.
'''
def suma_elementos(L):
    suma = 0
    for i in range(len(L)):
        suma += L[i]
    return suma