def mostrar_digitos(N):
    menor = 9
    mayor = 0
    while(N > 0):
        digito = N % 10
        N = N // 10
        
        if(digito < menor):
            menor = digito
        
        if(digito > mayor):
            mayor = digito
            
    print("Digito mayor del numero: ", mayor)
    print("Digito menor del numero: ", menor)

def main():
    mostrar_digitos(65468822110)
    mostrar_digitos(22121225463)
    
main()