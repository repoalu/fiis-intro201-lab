# n! = 1 x 2 x 3 x 4 x ... x n 
def factorial(n):
    fact = 1
    for i in range(1, n + 1):
        fact = fact * i
    return fact

def main():
    casos = [1, 5, 6, 20]
    for i in range(len(casos)):
        print(casos[i], factorial(casos[i]))

#main()