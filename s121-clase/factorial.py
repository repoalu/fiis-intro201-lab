def factorial(n):
    respuesta = None
    if(n == 0):
        respuesta = 1
    else:
        respuesta = n * factorial(n - 1)
    return respuesta

def main():
    print(factorial(6))

main()