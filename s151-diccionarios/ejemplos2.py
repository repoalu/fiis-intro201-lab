notas = {"Alvarez": 16, "Benitez": 18, "Zarate": 13}
print(notas)

notas["Moreno"] = 17
print(notas)

notas["Hurtado"] = 15
print(notas)

for nota in notas.values():
    print(nota)