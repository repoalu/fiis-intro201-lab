#Implemente una funcion que reciba un valor N y lea N valores de teclado

def leer_valores():
    lista = []
    continuar = True
    while(continuar == True):
        valor = input("Ingrese valor: ")
        if(valor == "ya"):
            continuar = False
        else:
            numero = int(valor)
            lista.append(numero)
    return lista

def es_cuadrado_perfecto(N):
    respuesta = False
    if(N ** (0.5) % 1 == 0):
        respuesta = True
    return respuesta

#16
# 1 ** 2, 2 ** 2, 3 ** 2, 4 ** 2 --> 16

def es_cuadrado_perfecto1(N):
    valor = 1
    respuesta = False
    while(valor * valor <= N):
        if(valor * valor == N):
            respuesta = True
        valor = valor + 1
    return respuesta

def filtrar_cuadrados(lista):
    respuesta = []
    for i in range(len(lista)):
        if(es_cuadrado_perfecto(lista[i]) == True):
            respuesta.append(lista[i])
    return respuesta

def main():
    lista = leer_valores()
    print(lista)
#Pruebas
#main()
print(es_cuadrado_perfecto1(40))


'''
Implemente dos funciones:
- La primer leera valores numericos desde teclado hasta 
que el usuario digite la palabra "ya". Los almacenara en
una lista.

- La segunda recibira una lista y retornara una nueva
lista solamente con los elementos que son cuadrados
perfectos
'''