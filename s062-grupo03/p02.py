#Obtener una nueva lista con los elementos de 3 cifras
def obtener_elementos(lista):
    respuesta = []
    for i in range(len(lista)):
        if(lista[i] >= 100 and lista[i] < 1000):
            respuesta.append(lista[i])
    return respuesta

def main():
    lista = [54, 125, 555, 12, 1677, 963]
    print(obtener_elementos(lista))

#
main()