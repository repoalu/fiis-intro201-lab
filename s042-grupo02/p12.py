#   1 +   1 +   2 +   4    +  7 +  13 + ... 
# prim   seg   terc  valor
#        prim  seg   terc    valor
def calcular_suma_serie(n):
    suma = 4
    primero = 1
    segundo = 1
    tercero = 2
    for i in range(n):
        valor = primero + segundo + tercero
        print(valor, end = " ")
        suma = suma + valor
        primero = segundo
        segundo = tercero
        tercero = valor
    print()
    return suma

def main():
    k = 12
    suma = calcular_suma_serie(12)
    print("Suma de terminos: ", suma)
main()