'''
Implemente un programa que, dados dos numeros enteros 
a y b, imprima en pantalla el listado de los numeros
perfectos en el intervalo [a, b]
'''
a = 1
b = 1000

for i in range(a, b + 1):
    suma_div = 0
    for j in range(1, i):
        if(i % j == 0):
            suma_div = suma_div + j
    if(suma_div == i):
        print(i, end = " ")