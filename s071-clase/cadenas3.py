def obtener_digitos(cad):
    respuesta = ""
    for i in range(len(cad)):
        if(cad[i].isdigit() == True): #cad[i].isdigit()
            respuesta += cad[i]
    return respuesta

def obtener_digitos1(cad):
    respuesta = ""
    digitos = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"]
    for i in range(len(cad)):
        es_digito = False
        for j in range(len(digitos)):
            if(cad[i] == digitos[j]):
                es_digito = True
        if(es_digito == True):
            respuesta += cad[i]
    return respuesta
    
def main():
    cadena = "AF5-458"
    digitos = obtener_digitos1(cadena)
    print(digitos)
#Test
main()
'''Implemente una funcion que reciba una cadena
y devuelva otra cadena considerando solamente aquellos
digitos que constituyen la cadena original
'''