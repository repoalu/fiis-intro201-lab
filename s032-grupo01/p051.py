def mostrar_comentario(calificacion):
    if(calificacion >= 0 and calificacion < 3): #[0, 3>
        print("En observacion")
    elif(calificacion >= 3 and calificacion < 4.5): #[3, 4.5>
        print("Bueno")
    elif(calificacion >= 4.5 and calificacion <=5): #[4.5, 5]
        print("Sobresaliente")
    else:
        print("Valor incorrecto de calificacion")

def main():
    mostrar_comentario(4)
    mostrar_comentario(4.5)
    mostrar_comentario(2.3)
    mostrar_comentario(1.9)
    mostrar_comentario(12)
#Llamada
main()