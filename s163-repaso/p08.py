def evaluar_condicion(cadena):
    res = False
    i = 0
    j = len(cadena) - 1
    contador = 0
    while(i <= j):
        if(cadena[i] != cadena[j]):
            contador = contador + 1
        i = i + 1
        j = j - 1 
    if(contador <= 1):
        res = True
    return res

def obtener_subcadenas(cadena):
    resp = []
    #Marca el inicio de cada subcadena
    for i in range(len(cadena)):
        #Marca el fin de cada subcadena
        for j in range(i + 1, len(cadena) + 1):
            #Agregando subcadena a la lista
            resp.append(cadena[i : j])
    return resp

#Funcion principal. Obtiene las subcadenas que cumplen la condicion
def contar_subcadenas(cadena):
    contador = 0
    lista = obtener_subcadenas(cadena)
    #Evaluamos cada subcadena
    for i in range(len(lista)):
        subcadena = lista[i]
        #Llamamos a la evaluacion por cada subcadena
        if(evaluar_condicion(subcadena) == True):
            contador = contador + 1
    return contador

def main():
    cadena = "ABBACDEF"
    print(evaluar_condicion(cadena))
    print(contar_subcadenas(cadena))
#Pruebas
main()
