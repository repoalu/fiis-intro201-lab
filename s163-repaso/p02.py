def mostrar_monedas(monto, denominaciones, cantidades):
    i = len(denominaciones) - 1
    monto = int(monto * 100)
    for i in range(len(denominaciones)):
        denominaciones[i] = int(denominaciones[i] * 100)

    while(monto != 0 and  i >= 0):
        monedas = monto // denominaciones[i]
        if(monedas > cantidades[i]):
            #Uso todas las monedas
            monedas = cantidades[i]
        print(f"Se requiere(n) {monedas} moneda(s) de {denominaciones[i]} ")
        monto = monto - denominaciones[i] * monedas
        print("Queda por repartir: ", monto)
        i = i - 1

def main():
    denominaciones = [0.1, 0.2, 0.5, 1]
    cantidades = [4, 4, 2, 1]
    monto = 2.2
    mostrar_monedas(monto, denominaciones, cantidades)

#Pruebas
main()
        
        