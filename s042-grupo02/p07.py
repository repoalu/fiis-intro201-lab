#k = 3 ..... 1 2 10 11 12 20 21 22 30
def mostrar_secuencia(N, k):
    #Numero en base k
    ultimo = 0
    for i in range(1, N + 1):
        if(ultimo % 10 == k - 1):
            ultimo = ultimo + 10 - (k - 1)
        else:
            ultimo = ultimo + 1
        print(ultimo, end = " ")

def main():
    N = 18
    k = 7
    mostrar_secuencia(N, k)

#Pruebas
main()