def fibo(n):
    if(n == 1):
        return 0
    elif(n == 2):
        return 1
    else:
        return fibo(n - 1) + fibo(n - 2)

def main():
    valores = [5, 9, 15, 22]
    for i in range(len(valores)):
        print(valores[i], fibo(valores[i]))

main()