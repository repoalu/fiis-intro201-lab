'''
[a0, a1, a2 ... an-1] --> a0 * x ^ (n - 1) + a1 * x ^ (n - 2) +
... + an-1
'''
def evaluar_polinomio(coef, valor):
    res = 0
    for i in range(len(coef)):
        exp = len(coef) - 1 - i
        res += (coef[i] * (valor ** exp))
    return res

def main():
    lista = [4, 5, 9]
    res = evaluar_polinomio(lista, 15)
    print(res)

#main()

