def repetitivas():
    n = 100
    sumaPar = 0
    sumaImpar = 0
    for i in range(1, n + 1):
        if(i % 2 == 0):
            sumaPar += i
        else:
            sumaImpar += i
    print(sumaPar, sumaImpar)

def elementos():
    lista = [1, 2, 3, 4]
    n = len(lista)
    for i in range(n):
        print("Elemento:", lista[i])

#5 * 3 ..... 4.5 * 8 
def multiplicar(a, b):
    return a * b;

def saludar(nombre):
    respuesta = "Hola " + nombre
    return respuesta

def main():
    res = saludar("Carlos")
    print(res)
#Pruebas
main()