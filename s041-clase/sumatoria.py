'''
Grupo de Ejercicios 01 - Ejercicio 12 (General)
'''
n = -1
while(n <= 0):
    n = int(input("Ingrese el valor de n: "))
suma = 0
for i in range(1, n + 1): #[1, n + 1>
    factor = i ** 5 + 4 * i ** 3 + 3 * i ** 2 + 5
    suma = suma + factor
print("La suma es: ", suma)
