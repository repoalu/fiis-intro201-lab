def colina_mas_alta(lista):
    #Longitud maxima de colina
    max_long = 0
    for i in range(1, len(lista) - 1):
        #Buscamos las 'cimas' de colina
        if(lista[i - 1] < lista[i] > lista[i + 1]):
            longitud = 0
            #Recorremos hacia la izquierda para contar elementos
            while(i >= 0 and lista[i] > lista[i - 1]):
                longitud = longitud + 1
                i = i - 1
            #Volvemos el contador a su posicion (elemento 'cima')
            i = i + longitud
            #Recorremos hacia la derecha para contar elementos
            while(i < len(lista) and lista[i] > lista[i + 1]):
                longitud = longitud + 1
                i = i + 1
            #Agregamos el elemento de la cima al conteo
            longitud = longitud + 1
            if(longitud > max_long):
                max_long = longitud
    return max_long

def main():
    lista = [2, 1, 4, 7, 3, 2, 5]
    print("Lista", lista)
    res = colina_mas_alta(lista)
    print("Longitud max: ", res)
    
main()