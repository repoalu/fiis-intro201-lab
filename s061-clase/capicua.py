def es_capicua(lista):
    inicio = 0
    fin = len(lista) - 1
    respuesta = True
    while(inicio <= fin):
        if(lista[inicio] != lista[fin]):
            respuesta = False
        inicio = inicio + 1
        fin = fin - 1
    return respuesta
'''
def main():
    lista = [1, 5, 6, 8, 6, 5, 1]
    print(es_capicua(lista))
#Pruebas
main()
'''

'''
Los digitos de un entero N se encuentran almacenados 
en una lista L. Implemente una funcion que reciba L
y permita determinar si el numero asociado es capicua
o no. La funcion debe retornar verdadero o falso
L = [1, 5, 1] ---> 151 (es capicua)
L = [3, 5, 6, 2] ---> 3562 (no es capicua)
'''