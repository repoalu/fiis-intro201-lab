'''
Implemente un programa que lea de pantalla el precio
unitario de un producto, la cantidad de unidades e 
imprima el precio total por pagar (incluya el 18%
de impuestos)
'''
precio_unitario = float(input("Ingrese precio unit: "))
unidades = int(input("Ingrese cant. unidades: "))
subtotal = precio_unitario * unidades
total = 1.18 * subtotal
print("Monto total: ", total)

