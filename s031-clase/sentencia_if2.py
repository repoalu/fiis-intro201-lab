''' 
Implemente un programa que permita mostrar una recomendacion
al alumno en funcion a la nota ingresada por teclado:
- [0, 5]: Necesita ayuda urgente.
- <5, 10]: Debe seguir practicando.
- <10, 15]: Siga asi, no se descuide.
- <15, 20]: Muy bien, continue el buen trabajo 
'''
nota = int(input("Ingrese nota: "))

if(nota >= 0 and nota <= 5): #(0 <= nota <= 5)
    print("Necesita ayuda urgente")
elif(nota > 5 and nota <= 10):
    print("Debe seguir practicando")
elif(nota > 10 and nota <= 15):
    print("Siga asi. No se descuide")
elif(nota > 15 and nota <= 20):
    print("Muy bien. Continue el buen trabajo")
else:
    print("Valor incorrecto para nota")
    


