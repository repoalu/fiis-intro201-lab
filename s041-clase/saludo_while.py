'''
Leer un valor "n" del teclado e imprimir el saludo "n" veces
'''
n = int(input("Ingrese valor de 'n': "))

contador = 1
while(contador <= n):
    print("Hola mundo ", contador)
    contador = contador + 1
