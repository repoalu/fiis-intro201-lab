import servicio
import creditos
def separador():
    LINEAS = 2
    REP = 40
    print()
    for i in range(LINEAS):
        print("*" * REP)
    print()
def mostrar_menu():
    opcion = None
    while(opcion != 3):
        separador()
        print("Bienvenido a la aplicacion")
        print("1. Ordenar servicio")
        print("2. Creditos")
        print("3. Salir")
        opcion = int(input("Ingrese la opcion requerida: "))
        separador()
        if(opcion == 1):
            servicio.ordenar()
        elif(opcion == 2):
            creditos.mostrar()
        elif(opcion == 3):
            print("Gracias por utilizar la aplicacion")
            break
        else:
            print("Ingrese una opcion correcta")
        input("Pulse enter para continuar... ")
