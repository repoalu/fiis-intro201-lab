def calcular_antiguedad(diac, mesc, anioc, dia, mes, anio):
    antiguedad = anio - anioc
    #Todavia no cumple el anio (comparacion de mes)
    if(mesc > mes or (mesc == mes and diac > dia)):
        antiguedad = antiguedad - 1
    return antiguedad

def main():
    #Leer valores
    dia = int(input("Digite fecha ingreso (dia): "))
    mes = int(input("Digite fecha ingreso (mes): "))
    anio = int(input("Digite fecha ingreso (anio): "))
    salario = float(input("Digite salario del empleado: "))
    #Calcular antiguedad
    antiguedad = calcular_antiguedad(dia, mes, anio, 6, 7, 2020)
    #Aplicar reglas
    if(antiguedad > 4):
        salario = 1.2 * salario
    elif(antiguedad >= 1):
        salario = 1.1 * salario
    print("Nuevo salario: ", salario)
    
main()