def contar_cifras(N):
    contador = 0
    while(N > 0):
        N = N // 10
        contador = contador + 1
    return contador

def main():
    print(contar_cifras(700))
    print(contar_cifras(1216546))

#Pruebas
main()