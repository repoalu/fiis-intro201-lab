def generar_lista(L):
    L1 = []
    for i in range(len(L)):
        res = L[i] ** 2 + 5
        L1.append(res)
    return L1

def main():
    L = [10, 23, 4, 1, 34]
    L1 = generar_lista(L)
    print(L)
    print(L1)

main()