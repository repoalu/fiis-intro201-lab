def reemplazar(texto, palabras, texto_reemp):
    for i in range(len(palabras)):
        palabra = palabras[i]
        texto = texto.replace(palabra, texto_reemp)
    return texto

texto = "Carlos me indico que habia coordinado con Pedro"
palabras = ["Carlos", "Pedro"]
nuevo = reemplazar(texto, palabras, "###")
print(nuevo)