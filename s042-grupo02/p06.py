def es_valido(N, k):
    respuesta = True
    while(N > 0):
        digito = N % 10
        N = N // 10
        if(digito >= k):
            respuesta = False
    return respuesta

def main():
    N = 4564654
    k = 8
    respuesta = es_valido(N, k)
    if(respuesta == True): #if(respuesta):
        print("Valor N puede representarse en base k")
    else:
        print("Valor N no puede representarse en base k")    
#Pruebas
main()