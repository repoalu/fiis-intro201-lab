def es_digito_primo(n):
    primo = False
    if(n == 2 or n == 3 or n == 5 or n == 7):
        primo = True
    return primo

def evaluar_digitos(n):
    factor = 1
    numero = 0
    while(n > 0):
        dig = n % 10
        if(es_digito_primo(dig) == True):
            numero = numero + dig * factor
            factor = factor * 10
        n = n // 10
    return numero ** 2


def main():
    N = 72836
    res = evaluar_digitos(N)
    print("Numero formato por digitos primos (izq a der) al cuadrado: ", res)
#Test
main()