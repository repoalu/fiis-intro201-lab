def obtener_serie_fibo(n):
    fib = [0] * n
    fib[0] = 0
    fib[1] = 1
    for i in range(2, n):
        fib[i] = fib[i - 1] + fib[i - 2]
    return fib

def main():
    print(obtener_serie_fibo(15))

main()