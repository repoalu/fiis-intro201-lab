continuar = True
lista = []
while(continuar == True):
    valor = float(input("Ingrese valor: "))
    if(valor > 0):
        lista.append(valor)
    else:
        continuar = False

maximo = None
segundoMax = None  
suma = 0
for i in range(len(lista)):
    if(maximo == None  or lista[i] > maximo):
        segundoMax = maximo
        maximo = lista[i]
    elif(segundoMax == None or lista[i] > segundoMax):
        segundoMax = lista[i]
    suma = suma + lista[i]

print("Precio mayor: ", maximo)
print("Segundo mayor: ", segundoMax)
promedio = suma / len(lista)
print("Promedio: ", promedio)

contador = 0
for i in range(len(lista)):
    if(lista[i] > promedio):
        contador = contador + 1
print("Porcentaje mayores que el promedio: ", contador / len(lista) * 100)