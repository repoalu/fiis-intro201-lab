'''
Calcule el promedio de 2 notas ingresadas por teclado.
Lea las participaciones y si tiene mas de 10 agregue
un punto al promedio.
Muestre un mensaje de aprobado si el promedio es > 10
'''

n1 = int(input("Ingrese nota1: "))
n2 = int(input("Ingrese nota2: "))
part = int(input("Ingrese cant. participaciones: "))

promedio = (n1 + n2) / 2
print("El promedio es: ", promedio)

if(part > 10):
    promedio = promedio + 1

if(promedio > 10):
    print("Alumno aprobado")
    print("Felicitaciones!")
else:
    print("Alumno desaprobado")
    print("Puede mejorar. A estudiar!")
