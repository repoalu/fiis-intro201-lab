#Importamos la funcion para obtener las subcadenas
from funciones import obtener_subcadenas

def contar_subcadenas(txt, str):
    #Obtenemos las subcadenas con lo trabajado 
    lista = obtener_subcadenas(txt)
    contador = 0
    for i in range(len(lista)):
        subcadena = lista[i]
        #En caso el patron no se encuentre en la cadena, "find" retorna - 1
        if(subcadena.find(str) != -1):
            print(f"Subcadena '{subcadena}' contiene a '{str}'")
            contador = contador + 1
    return contador

def main():
    txt = "abatxt"
    str = "at"
    cantidad = contar_subcadenas(txt, str)
    print("Cantidad de subcadenas: ", cantidad)
    
#Pruebas
main()