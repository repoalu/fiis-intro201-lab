def obtener_subcadenas(cadena):
    resp = []
    #Marca el inicio de cada subcadena
    for i in range(len(cadena)):
        #Marca el fin de cada subcadena
        for j in range(i + 1, len(cadena) + 1):
            #Agregando subcadena a la lista
            resp.append(cadena[i : j])
    return resp