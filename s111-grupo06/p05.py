import p04

def calcular_suma(n):
    resultado = 0
    for i in range(1, n + 1):
        resultado = resultado + p04.calcular_expresion(i)
    return resultado

def main():
    print(calcular_suma(5))

main()