def cumple(n):
    cond = False
    for i in range(n):
        if(i * (i + 1) == n):
            cond = True
    return cond

def evaluar(n):
    contador = 0
    for i in range(n):
        valor = int(input("Ingrese valor: "))
        if(cumple(valor) == True):
            contador = contador + 1
    return contador

def main():
    n = int(input("Ingrese cantidad de valores a leer: "))
    conteo = evaluar("Cantidad de valores que cumplen la condicion: ", n)
    print(conteo)
#Test
main()