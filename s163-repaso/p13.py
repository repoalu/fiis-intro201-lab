calificaciones = {"08956859": [3, 3, 5],
                  "45844589": [5]
                  }
def registrar_calificaciones():
    continuar = True
    while(continuar == True):
        print(calificaciones)
        respuesta = input("Para registrar una calificacion presione 'S': ")
        if(respuesta == "S"):
            dni = input("Ingrese DNI: ")
            calif = int(input("Ingrese calificacion: "))
            if(dni in calificaciones):
                calificaciones[dni].append(calif)
            else:
                calificaciones[dni] = [calif]
        else:
            continuar = False

def mostrar_reporte():
    #Por cada DNI de conductor  ---- keys(), values()
    for conductor in calificaciones.keys():
        lista = calificaciones[conductor]
        suma = 0
        menor = 5
        for i in range(len(lista)):
            suma += lista[i]
            if(lista[i] < menor):
                menor = lista[i]
        if(len(lista) > 1):
            promedio = (suma - menor) / (len(lista) - 1)
        else:
            promedio = suma
        print(f"Conductor: {conductor}, Promedio: {promedio}")

def modificar_calificacion():
    dni = input("Ingrese DNI para modificar calificacion: ")
    lista = calificaciones[dni]
    print("Las calificaciones son: ", lista)
    indice = int(input(f"Ingrese la calificacion (del 1 al {len(lista)}) que desea modificar: "))
    print("Valor anterior: ", lista[indice - 1])
    valor = int(input("Ingrese nuevo valor: "))
    lista[indice - 1] = valor
    print(calificaciones)
    
    
def main():
    #registrar_calificaciones()
    #mostrar_reporte()
    modificar_calificacion()
#Pruebas
main()