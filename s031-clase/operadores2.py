n = 15
m = 25
z = 52

print((n != 10) and (z % 2 == 0))
print((n % 5 == 0) or (m < 20))
print(not(n % 5 == 0) and not(n % 3 == 0))
