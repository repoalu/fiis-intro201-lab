'''
Implemente un programa que permita imprimir un patron
de matriz de N x N, dado el valor "N" como dato.
Ejemplo:
N = 3   
* * *
* * *
* * *
'''
N = 5
for i in range(N): #0 1 2 3 4
    for j in range(N):
        print("*", end = " ")
    print()

