n = int(input("Ingrese valor de 'n': "))
menor = None
segundoMenor = None
negativo = False
suma = 0
for i in range(n):
    valor = int(input("Ingrese un entero positivo: "))
    if(valor < 0 ):
        negativo = True
        break
    else:
        suma = suma + valor
        if(menor == None or valor < menor):
            segundoMenor = menor
            menor = valor
        elif(segundoMenor == None or valor < segundoMenor):
            segundoMenor = valor

if(negativo == True):
    print("Error: Se ingreso un valor negativo")
else:
    promedio = (suma - menor - segundoMenor) / (n - 2)
    print("Promedio: ", promedio)