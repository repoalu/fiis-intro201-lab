import math
'''
Alternativas para hallar la longitud:
1) digitos = int(math.log10(N)) + 1
2) print(len(str(N)))
'''
def es_primo_ti(N):
    resultado = True
    contador = 0
    aux = N
    while(aux > 0):
        aux = aux // 10
        contador = contador + 1
    
    for i in range(contador):
        numero = N % (10 **(i + 1))
        divisores = 0
        for j in range(1, numero + 1):
            if(numero % j == 0):
                divisores = divisores + 1
        if(divisores == 2):
            print(numero, "Primo")
        else:
            print(numero, "No primo")
            resultado = False
    return resultado

def main():
    N = 2617
    resultado = es_primo_ti(N)
    print(resultado)
main()