import capicua

def convertir_lista():
    nombre = input("Ingrese nombre: ")
    lista = [int(x) for x in list(nombre)]
    return lista

def convertir_lista1():
    lista = []
    N = int(input("Ingrese numero: "))
    while(N > 0):
        dig = N % 10
        lista.append(dig)
        N = N // 10
    lista = invertir(lista)
    return lista

def invertir(lista):
    respuesta = []
    i = len(lista) - 1
    while(i >= 0):
        respuesta.append(lista[i])
        i = i - 1
    return respuesta

def main():
    lista = convertir_lista1()
    res = capicua.es_capicua(lista)
    if(res == True):# if(res):
        print("Es capicua")
    else:
        print("No es capicua")
        
#Test
main()
'''
Implemente una funcion que permita leer desde el teclado
un numero entero N y devuelva una lista con los digitos
de dicho numero.
Luego utilice su funcion implementada en capicua.py
para validar si dicho numero es capicua o no.
Ejemplo:
N = 875
Respuesta = [8, 7, 5]
'''