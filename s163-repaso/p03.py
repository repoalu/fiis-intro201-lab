def es_diagonal(matriz):
    res = True
    for i in range(len(matriz)):
        for j in range(len(matriz[0])):
            '''
            Condiciones:
            1) No esta en la diagonal principal --> i != j
            and
            2) Es diferente de cero --> matriz[i][j] != 0
            '''
            if(i != j and matriz[i][j] != 0): 
                res = False
    return res

def main():
    matriz = [ [1, 0, 0],
               [0, 8, 0],
               [0, 0, 9] ]
    resp = es_diagonal(matriz)
    print("Es diagonal: ", resp)
    
#Pruebas
main()