def sumar_cifras(N):
    suma = 0
    while(N > 0):
        suma = suma + N % 10
        N = N // 10
    return suma

def main():
    print(sumar_cifras(654654))
    print(sumar_cifras(5545221333))

#Pruebas
main()