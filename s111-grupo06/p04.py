import p03
import p02

def obtener_lista_primos(n):
    cantidad = 0
    resp = []
    i = 1
    while(cantidad < n):
        if(p02.es_primo(i)):
            resp.append(i)
            cantidad = cantidad + 1
        i = i + 1
    return resp

def calcular_expresion(n):
    # Obtener lista de numeros primos
    L = obtener_lista_primos(n)
    total = 0
    # Recorrer la lista L
    for i in range(len(L)):
        #Por cada elemento L[i] calcular 1 / L[i]!
        res = 1 / p03.factorial(L[i])
        #Agregar el resultado al total
        total = total + res
    return total

def main():
    casos = [3, 5, 8, 12]
    for i in range(len(casos)):
        valor = calcular_expresion(casos[i])
        print(casos[i], valor)

#main()