def cambiar1(numero):
    numero = numero * 200

def cambiar2(cadena):
    cadena = cadena + "ABC"

def cambiar3(lista):
    lista.append(100)
    
def cambiar4(cadena):
    nuevo = cadena + "ABC"
    return nuevo

numero = 5
cambiar1(numero)
print(numero)

cadena = "XYZ"
cambiar2(cadena)
print(cadena)

lista = [1, 2, 3]
cambiar3(lista)
print(lista)

'''
Implemente una funcion que permita agregar "ABC" al final 
de una cadena dada como dato
'''
original = "Cadena"
original = cambiar4(original)
print(original)


