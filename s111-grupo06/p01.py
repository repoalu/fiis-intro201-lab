def cantidad_divisores(n):
    cantidad = 0
    for i in range(1, n + 1): #1, 2, 3, 4, ..., n - 1, n
        if(n % i == 0):
            #Es un divisor
            cantidad = cantidad + 1
    return cantidad

def main():
    cant = cantidad_divisores(36)
    print(cant)

#main()